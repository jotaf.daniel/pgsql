insert into
disciplinas(codigo, nome, cr_aula, link, periodo)
values
('MAC0110', 'Introducao a computacao', 4, 'vazio', '1o'),
('MAC0121', 'Algoritmos e Estruturas de dados I', 4, 'vazio', '2o');

insert into
disciplinas(codigo, nome, cr_aula, cr_trabalho, link, periodo)
values
('MAC0323', 'Algoritmos e Estruturas de dados II', 4, 2, 'vazio', '3o');

insert into
prerequisitos(pai_id, filho_id, forca, curso)
values
(1, 2, 1, 'BCC'),
(2, 3, 1, 'BCC');

insert into
prerequisitos(pai_id, filho_id, forca, curso)
values
(1, 2, 1, 'BCC'),
(2, 3, 1, 'BCC');
