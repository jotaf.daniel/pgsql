select * -- disciplinas.id, disciplinas.codigo, prerequisitos.pai_id
from disciplinas
inner join prerequisitos
on disciplinas.id = prerequisitos.filho_id;

-- left join
-- right join
-- inner join
-- full outer join


-- para cada disciplina que TEM prerequisito,
-- mostrar o código da disciplina e do(s) seu(s) pai(s)

select filhos.codigo, pais.codigo
from disciplinas as filhos
inner join prerequisitos as pr
on filhos.id = pr.filho_id
-- output do terminal
inner join disciplinas as pais
on pr.pai_id = pais.id;