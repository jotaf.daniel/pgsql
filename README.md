# pSQL

A study about postgresql.

![data models][/modelos_dados.png]

## Running

The Postgres Docker image runs an instance of the database as default command, and we need to access it _locally_. To achieve that, run

```bash
# run a postgres container in background -- it'll start the DB
docker-compose up -d

# launch a BASH inside the same container
docker-compose exec pgsql bash

# change to the mountpoint
cd playground

# run a SQL file as a script inside the database
psql -U $POSTGRES_USER -d $POSTGRES_DB -f <filename>
```

The provided script files must be ran in a certain order

1. `create_tables.sql`
2. `populate_tables.sql`
3. `query.sql`
