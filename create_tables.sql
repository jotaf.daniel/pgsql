drop table prerequisitos;
drop table disciplinas;

create table disciplinas (
  id serial primary key,
  codigo varchar(7) unique,
  nome varchar(255),
  cr_aula integer,
  cr_trabalho integer default 0,
  link varchar(255),
  periodo varchar(100)
);

create table prerequisitos (
  id serial primary key,
  pai_id integer references disciplinas(id),
  filho_id integer references disciplinas(id),
  forca integer, -- 1: forte, 0: fraco
  curso varchar(255),
  unique(pai_id, filho_id)
  -- distinct(pai_id, filho_id)
);
